"use client";
import {
  TextInput,
  PasswordInput,
  Anchor,
  Paper,
  Title,
  Text,
  Container,
  Button,
} from "@mantine/core";
import classes from "../../styles/AuthenticationTitle.module.css";
import { signIn } from "next-auth/react";
import { useState } from "react";
import Link from "next/link";
import React from "react";
import { toast } from "react-toastify";

const AuthenticationTitle = () => {
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (!email) {
      toast.warning("Please enter your email");
      return;
    }
    if (!password) {
      toast.warning("Please enter your password");
      return;
    }
    try {
      const result = await signIn("credentials", {
        email,
        password,
        redirect: false,
      });
      if (result?.status === 200) {
        window.location.href = "/";
      } else {
        toast.error("Login failed: Your email or password is incorrect!");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Container size={420} my={40}>
      <Title ta="center" className={classes.title}>
        Sign In
      </Title>
      <Text c="dimmed" size="sm" ta="center" mt={5}>
        Do not have an account yet?{" "}
        <Anchor size="sm" component="button">
          <Link href={"/auth/register"}>Create account</Link>
        </Anchor>
      </Text>
      <Paper withBorder shadow="md" p={30} mt={30} radius="md">
        <TextInput
          type="email"
          label="Email"
          placeholder="Email"
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <PasswordInput
          label="Password"
          placeholder="Your password"
          required
          onChange={(e) => setPassword(e.target.value)}
          mt="md"
        />
        <Button fullWidth mt="xl" onClick={(e) => onSubmit(e)}>
          Sign in
        </Button>
      </Paper>
    </Container>
  );
};
export default AuthenticationTitle;
