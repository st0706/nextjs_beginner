"use client";
import { PasswordInput, TextInput, Button, Group, Box } from "@mantine/core";
import { ChangeEvent, useState } from "react";
import { createNewUserService } from "@/app/services/userService";

function Register() {
  let [formValues, setFormValues] = useState({
    name: "",
    email: "",
    password: "",
    passwordConfirm: "",
  });

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    createNewUserService(formValues);
  };

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Register</h1>
      <Box maw={340} mx="auto" my="lg">
        <form onSubmit={onSubmit}>
          <TextInput
            required
            withAsterisk
            label="Name"
            name="name"
            value={formValues.name}
            placeholder="Your name"
            onChange={handleChange}
            style={{ padding: "1rem" }}
          />

          <TextInput
            type="email"
            required
            withAsterisk
            label="Email"
            placeholder="youremail@email.com"
            name="email"
            value={formValues.email}
            onChange={handleChange}
            style={{ padding: "1rem" }}
          />

          <PasswordInput
            required
            label="Password"
            name="password"
            placeholder="Password"
            value={formValues.password}
            onChange={handleChange}
            style={{ padding: "1rem" }}
          />

          <PasswordInput
            required
            label="Confirm password"
            name="passwordConfirm"
            placeholder="Confirm password"
            onChange={handleChange}
            value={formValues.passwordConfirm}
            style={{ padding: "1rem" }}
          />

          <Group justify="center" mt="md">
            <Button type="submit">Register</Button>
          </Group>
        </form>
      </Box>
    </>
  );
}
export default Register;
