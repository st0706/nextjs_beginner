import { toast } from "react-toastify";
import { signIn } from "next-auth/react";

const createNewUserService = async (formValues: any) => {
    try {
        if (formValues.password !== formValues.passwordConfirm) {
          toast.error("Your password is not confirmed!");
          return;
        }
        const res = await fetch("/api/register", {
          method: "POST",
          body: JSON.stringify(formValues),
          headers: {
            "Content-Type": "application/json",
          },
        });
  
        if (!res.ok) {
          toast.error('Register failed: Your email is existed!');
          return;
        }
  
        signIn(undefined, { callbackUrl: "/" });
      } catch (error: any) {
        console.error(error);
      }
}

export {
    createNewUserService,
}